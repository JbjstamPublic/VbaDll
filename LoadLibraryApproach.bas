Attribute VB_Name = "LoadLibraryApproach"
Public Declare PtrSafe Function DoSomethingWithSheet Lib "TestDll.dll" (ByRef sheetObj) As Boolean
Public Declare PtrSafe Function DoSomethingWithSheetNetOffice Lib "TestDll.dll" (ByRef sheetObj) As Boolean
Public Declare PtrSafe Function GetFilesByExtensions Lib "TestDll.dll" (ByRef filesRef, ByVal folderPath As String, ByVal extensions, ByVal includeSubdirs As Boolean) As Boolean

Private Declare PtrSafe Function FreeLibrary Lib "kernel32" (ByVal hLibModule As Long) As Long
Private Declare PtrSafe Function LoadLibraryA Lib "kernel32" (ByVal lpLibFileName As String) As Long

Public Declare PtrSafe Function FilterStringArray Lib "TestDll.dll" (ByRef myArrRef, ByVal CSharpLambdaExpression As String) As Boolean

Sub TestFilterStringArray()
    Dim myArr() As String
    Dim i As Long
    Dim moduleHandle As Long
    
    On Error GoTo restore
    
    moduleHandle = LoadLibrary("TestDll.dll")
    
    myArr = Split("johan JOHAN johannnn JOHANNNNN hAha HAHA")
    ''If FilterStringArray(myArr, "s => s.ToUpperInvariant().Equals(s, StringComparison.CurrentCulture)") Then
    If FilterStringArray(myArr, "s => s.ToCharArray().All(c => !(char.IsLower(c)))") Then
        For i = 0 To UBound(myArr)
            Debug.Print " - " & myArr(i)
        Next i
    Else
        Debug.Print "ERROR: " & myArr(0)
    End If
    
restore:
    If moduleHandle <> 0 Then
        Call FreeLibrary(moduleHandle)
    End If

End Sub

Sub TestFolderFiltering()
    Dim files() As String
    Dim i As Long
    Dim moduleHandle As Long
    
    On Error GoTo restore
    
    moduleHandle = LoadLibrary("TestDll.dll")
  
    If GetFilesByExtensions(files, "C:\Tradostest\Project 4", Split("sdlxliff", ";"), True) Then
        For i = 0 To UBound(files)
            Debug.Print " - " & files(i)
        Next i
    Else
        Debug.Print "ERROR: " & files(0)
    End If
    
restore:
    If moduleHandle <> 0 Then
        Call FreeLibrary(moduleHandle)
    End If

End Sub

Private Function LoadLibrary(dllName As String) As Long
    Dim path As String
    path = ThisWorkbook.path & "\" & dllName
    LoadLibrary = LoadLibraryA(path)
End Function
