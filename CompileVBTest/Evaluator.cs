﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Microsoft.CSharp;
using Microsoft.VisualBasic;

namespace StringCompilation
{
    public class Evaluator<TCompiler> where TCompiler : CodeDomProvider, new()
    {
        public object Eval(string vbCode, IEnumerable<string> imports = null, IEnumerable<string> references = null)
        {
            return Eval<object>(vbCode, imports, references);
        }

        public T Eval<T>(string vbCode, IEnumerable<string> imports = null, IEnumerable<string> references = null)
        {
            var code = GenerateEvalCode(vbCode, imports);

            var assembly = Compile(code, references);

            var method = GetStaticMethod(assembly, "TempNamespace.TempClass", "EvalCode");

            return (T)method.Invoke(null, null);
        }

        private MethodInfo GetStaticMethod(Assembly assembly, string fullClassName, string staticMethodName)
        {
            var type = assembly.GetTypes()
                .FirstOrDefault(t => t.FullName.Equals(fullClassName));

            if (type == null)
                throw new Exception("Couldn't find generated type");

            var method = type.GetMethod(staticMethodName, BindingFlags.Static | BindingFlags.Public);

            if (method == null)
                throw new Exception("Couldn't find generated method");

            return method;
        }

        private Assembly Compile(string code, params string[] references)
        {
            return Compile(code, (IEnumerable<string>)references);
        }
        private Assembly Compile(string code, IEnumerable<string> references)
        {
            var compiler = new TCompiler();
            var parameters = new CompilerParameters();

            var refs = references?.ToList() ?? new List<string>();
            AddIfNew(refs, "System.dll");

            parameters.ReferencedAssemblies.AddRange(refs.ToArray());
            parameters.CompilerOptions = "/t:library";
            parameters.GenerateInMemory = true;

            var compilerResults = compiler.CompileAssemblyFromSource(parameters, code);

            if (compilerResults.Errors.Count > 0)
                throw new Exception("compile errors: " + Environment.NewLine
                                    + string.Join(Environment.NewLine,
                                        compilerResults.Errors.Cast<CompilerError>()
                                            .Select(e => e.ToString())));

            return compilerResults.CompiledAssembly;
        }

        public TFunc CompileDelegate<TFunc>(string delegateExpression)
        {
            return CompileLambda<TFunc>(delegateExpression);
        }

        public TFunc CompileLambda<TFunc>(string lambdaExpression)
        {
            var code = GenerateCompileLambdaCode<TFunc>(lambdaExpression);

            var assembly = Compile(code, "system.dll", "System.Xml.Linq.dll",
                "System.Xml.dll", "System.Core.dll");

            var method = GetStaticMethod(assembly, "TempNameSpace.TempClass", "GetPredicate");

            return (TFunc)method.Invoke(null, null);
        }


        private string GenerateCompileLambdaCode<TFunc>(string lambdaExpression)
        {
            if (typeof(TCompiler) == typeof(CSharpCodeProvider))
            {
                return "using System;" + Environment.NewLine
                       + "using System.Collections.Generic;" + Environment.NewLine
                       + "using System.Linq;" + Environment.NewLine
                       + "namespace TempNameSpace {" + Environment.NewLine
                       + "    public static class TempClass {" + Environment.NewLine
                       + "        public static " + ResolveTFunc(typeof(TFunc)) + " GetPredicate() { return " +
                       lambdaExpression + "; }" + Environment.NewLine
                       + "    }" + Environment.NewLine
                       + "}" + Environment.NewLine;
            }
            if (typeof(TCompiler) == typeof(VBCodeProvider))
            {
                return
                    "Imports System.Collections.Generic" + Environment.NewLine +
                    "Imports System.Linq" + Environment.NewLine +
                    "Namespace TempNameSpace" + Environment.NewLine +
                    "	Public Module TempClass" + Environment.NewLine +
                    "		Public Function GetPredicate() As System.Func(Of System.Int32, System.Boolean)" + Environment.NewLine +
                    "			Return " + lambdaExpression +" " + Environment.NewLine +
                    "		End Function" + Environment.NewLine +
                    "	End Module" + Environment.NewLine + "End Namespace";
            }


            throw new Exception("Unsupported compiler");
        }

        private static string ResolveTFunc(Type type)
        {
            if (!type.BaseType.FullName.Equals("System.MulticastDelegate", StringComparison.InvariantCulture))
                throw new Exception("Not a func");

            var args = type.GenericTypeArguments.Select(t => t.FullName);

            return "Func<" + string.Join(", ", args) + ">";
        }

        private string GenerateEvalCode(string vbCode, IEnumerable<string> imports)
        {
            var imps = imports?.ToList() ?? new List<string>();

            AddIfNew(imps, "System");

            var compilerType = typeof(TCompiler);

            if (compilerType == typeof(VBCodeProvider))
            {
                return GenerateEvalCodeVb(vbCode, imps);
            }
            if (compilerType == typeof(CSharpCodeProvider))
            {
                return GenerateEvalCodeCSharp(vbCode, imps);
            }

            throw new Exception("Unsupported compiler type");
        }

        private string GenerateEvalCodeCSharp(string vbCode, IEnumerable<string> imports)
        {
            StringBuilder sb = new StringBuilder();

            foreach (var import in imports)
            {
                sb.Append("using ").Append(import).AppendLine(";");
            }

            sb.AppendLine("namespace TempNamespace {");
            sb.AppendLine("    public static class TempClass {");
            sb.AppendLine("        public static object EvalCode() {");
            sb.AppendLine("            return " + vbCode + ";");
            sb.AppendLine("        }");
            sb.AppendLine("    }");
            sb.AppendLine("}");

            return sb.ToString();
        }

        private string GenerateEvalCodeVb(string vbCode, IEnumerable<string> imports)
        {
            StringBuilder sb = new StringBuilder();

            foreach (var import in imports)
            {
                sb.Append("Imports ").AppendLine(import);
            }

            sb.AppendLine("Namespace TempNamespace");
            sb.AppendLine("    Module TempClass");
            sb.AppendLine("        public function  EvalCode() as Object");
            sb.AppendLine("            Return " + vbCode);
            sb.AppendLine("        End Function");
            sb.AppendLine("    End Module");
            sb.AppendLine("End Namespace");

            return sb.ToString();
        }

        private static void AddIfNew<T>(ICollection<T> collection, params T[] objects)
        {
            foreach (var obj in objects)
            {
                if (!collection.Contains(obj))
                    collection.Add(obj);
            }

        }
    }
}