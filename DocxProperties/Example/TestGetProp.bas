Attribute VB_Name = "TestGetProp"
Option Explicit

#If Win64 Then
    Private Declare PtrSafe Function LoadLibraryA Lib "kernel32" (ByVal lpLibFileName As String) As Long
    Private Declare PtrSafe Function GetDocxProp Lib "DocxProperties64.dll" (ByVal wordPath As String, ByVal propName As String) As String
#Else
    Private Declare Function LoadLibraryA Lib "kernel32" (ByVal lpLibFileName As String) As Long
    Private Declare Function GetDocxProp Lib "DocxProperties64.dll" (ByVal wordPath As String, ByVal propName As String) As String
#End If

Sub TestAuthorName()
    Dim dllPath As String
    
    #If Win64 Then
        dllPath = "DocxProperties64.dll"
    #Else
        dllPath = "DocxProperties32.dll"
    #End If
        
    Call LoadLibrary(dllPath)
    
    Debug.Print GetDocxProp(ThisWorkbook.path & "\EmptyDoc.docx", "creator")
    
End Sub

Private Function LoadLibrary(dllName As String) As Long
    Dim path As String
    path = ThisWorkbook.path & "\" & dllName
    LoadLibrary = LoadLibraryA(path)
End Function


