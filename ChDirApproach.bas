Attribute VB_Name = "ChDirApproach"
Option Explicit

Public Declare PtrSafe Function GetFilesWithExt Lib "TestDll.dll" Alias "GetFilesWithExtension" (ByVal folderPath As String, ByVal extension As String, ByVal includeSubdirs As Boolean) As String
Public Declare PtrSafe Function GetFilesByExtensions Lib "TestDll.dll" (ByRef filesRef, ByVal folderPath As String, ByVal extensions, ByVal includeSubdirs As Boolean) As Boolean

''Just some testing
'Public Declare PtrSafe Function Addition Lib "TestDll.dll" (ByVal a As Long, ByVal b As Long) As Long
'Public Declare PtrSafe Function LinqAddition Lib "TestDll.dll" (ByVal a As Long, ByVal b As Long) As Long
'Public Declare PtrSafe Function LinqAdditionString Lib "TestDll.dll" (ByVal a As Long, ByVal b As Long) As String

Public Declare PtrSafe Function DoSomethingWithSheet Lib "TestDll.dll" (ByRef sheetObj) As Boolean
Public Declare PtrSafe Function DoSomethingWithSheetNetOffice Lib "TestDll.dll" (ByRef sheetObj) As Boolean
Public oldDir As String

''This works when you dont need special ByRef/ByVal stuff
Function ExecuteFunc(functionName As String, Optional arg1 As Variant, Optional arg2 As Variant, Optional arg3 As Variant, Optional arg4 As Variant, Optional arg5 As Variant, Optional arg6 As Variant, Optional arg7 As Variant, Optional arg8 As Variant, Optional arg9 As Variant, Optional arg10 As Variant, Optional arg11 As Variant, Optional arg12 As Variant) As Variant
    SetWorkingDir
    On Error GoTo restoreit
    ExecuteFunc = Application.Run(functionName, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12)
restoreit:
    RestoreWorkingDir
End Function

Private Sub SetWorkingDir()
    oldDir = CurDir
    ChDir ActiveWorkbook.path
End Sub

Private Sub RestoreWorkingDir()
    ChDir oldDir
End Sub

Sub TestExcelFuncs()
    Dim oSheet As Worksheet
    Set oSheet = ActiveSheet
    
    Debug.Print ExecuteFunc("DoSomethingWithSheet", oSheet)
    Debug.Print ExecuteFunc("DoSomethingWithSheetNetOffice", oSheet)
    Debug.Print oSheet.Name
End Sub

Sub TestFolderFiltering()
    Dim files() As String
    Dim i As Long
    
    SetWorkingDir
    On Error GoTo restore
    
    If GetFilesByExtensions(files, "C:\Tradostest\Project 4", Split("sdlxliff", ";"), True) Then
        For i = 0 To UBound(files)
            Debug.Print " - " & files(i)
        Next i
    Else
        Debug.Print "ERROR: " & files(0)
    End If
    
restore:
    RestoreWorkingDir
End Sub

Function GetFilesWithExtension(folderPath As String, extension As String, includeSubdirs As Boolean) As String()
    GetFilesWithExtension = Split(GetFilesWithExt(folderPath, extension, includeSubdirs), ";")
End Function

