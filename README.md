# C# unmanaged dll for Visual Basic for Applications

## Description

This project illustrates how you can write DLL's in C# that can be used with VBA without any COM registering, using Public Declare imports.

### Thanks to

* [RGiesecke.DllExport](https://www.nuget.org/packages/UnmanagedExports/) for unmanaged function exports
* [Costura.Fody](https://www.nuget.org/packages/Costura.Fody/2.0.0-beta0018) for embedding everything into one assembly
* [CSScript](http://www.csscript.net/) for implementing C# lambda expressions to filter arrays
* [NetOffice](https://netoffice.codeplex.com/)
* [Stackoverflow](http://stackoverflow.com/questions/40043101/return-string-in-c-sharp-dll-for-vba) for the tip of passing objects by reference instead of returning them

### Files and methods of note
* **\TestDLL** - the DLL itself
    * ExcelFunctions.cs - Illustrates how to pass an object from VBA to C#, cast it to the corresponding Microsoft.Office.Interop.Excel or NetOffice type and use it
        * _DoSomethingWithSheet_
        * _DoSomethingWithSheetNetOffice_
    * FilterArray.cs - Filters VBA arrays via LINQ
        * _FilterStringArray_ - Filters a string array given a string representatoin of C# lambda or delegate expession
    * UnusedMethods.cs
        * Some simple examples using primitive return types (int, string)
    * FolderHandling.cs
        * _GetFilesByExtensions_ - Takes a folder path as a string and an array of allowed extensions, returns a string array of all matching files
* ChDirApproach.bas
    * Contains one possible way of using the DLL without placing it in \System (changing the working directory of Excel temporarily)
* LoadLibraryApproach.bas
    * Contains a better way of importing (using LoadLibrary and, optionally, FreeLibrary)
    * Examples of how to use various DLL functions
    

### Debug info
Debug info is occasionally output into a file named TestDll.dll.log in the folder where the DLL resides.

### Current calling conventions
* Most DLL functions return a boolean indicating success.
* Exceptions and other errors are returned as a string in the case of functions that return a string
* Exceptions and other errors are returned as a one-item string array in the case of functions that return a string array
* Currently no exceptions are returned when functions manipulating COMObjects directly encounter errors (but they are output to the log file)

### Known limitations
* Returning anything except primitive types seems difficult, so objects are passed back and forth by reference
* Passing more specific things than C# objects / VBA variants doesn't see to work well, so the DLL code uses runtime type checking before casting some parameters to their approporiate type
* The current code has not been made compatible with x86 (though this should be trivial to achieve)